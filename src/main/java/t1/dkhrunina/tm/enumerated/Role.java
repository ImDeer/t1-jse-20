package t1.dkhrunina.tm.enumerated;

public enum Role {

    USUAL("Regular user"), ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}