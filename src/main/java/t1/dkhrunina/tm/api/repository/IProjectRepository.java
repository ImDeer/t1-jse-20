package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}