package t1.dkhrunina.tm.api.model;

import t1.dkhrunina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}