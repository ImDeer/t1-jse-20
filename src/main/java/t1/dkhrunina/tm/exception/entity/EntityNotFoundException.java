package t1.dkhrunina.tm.exception.entity;

public final class EntityNotFoundException extends AbstractEntityException {

    public EntityNotFoundException() {
        super("Error: object not found");
    }

}