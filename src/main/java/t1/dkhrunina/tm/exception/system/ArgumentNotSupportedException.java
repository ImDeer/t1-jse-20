package t1.dkhrunina.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error: argument is not supported");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error: argument \"" + argument + "\" is not supported");
    }

}