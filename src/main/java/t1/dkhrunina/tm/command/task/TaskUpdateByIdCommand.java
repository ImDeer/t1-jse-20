package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "t-update-by-id";

    private static final String DESCRIPTION = "Update task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Update task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }

}