package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private static final String NAME = "t-unbind";

    private static final String DESCRIPTION = "Unbind task from project by project id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Unbind task from project]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

}