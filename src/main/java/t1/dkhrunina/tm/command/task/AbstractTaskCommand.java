package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.api.service.ITaskService;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        if (!task.getDescription().isEmpty()) System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Project id: " + (task.getProjectId() == null ? "not bound" : task.getProjectId()));
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getArgument() {
        return null;
    }

}