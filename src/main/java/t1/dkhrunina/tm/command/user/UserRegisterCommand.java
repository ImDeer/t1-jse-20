package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.api.service.IAuthService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    private static final String NAME = "u-register";

    private static final String DESCRIPTION = "Register new user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[User registration]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email: ");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        final User user = authService.register(login, password, email);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}